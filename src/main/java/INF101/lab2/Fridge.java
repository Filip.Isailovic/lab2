package INF101.lab2;


import javafx.css.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


public class Fridge implements IFridge {

    int maxSize=20;

    public List<FridgeItem> fridgeArray = new ArrayList<>();
 
    @Override
    public int nItemsInFridge() {
        return fridgeArray.size();
    }

    @Override
    public int totalSize() {
        return maxSize;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if(fridgeArray.size()>20) {
            fridgeArray.add(item);
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (fridgeArray.contains(item)) {
            fridgeArray.remove(item);
        }
        else {
            throw new NoSuchElementException("Couldn't find " + item + " in fridge");
        }
    }

    @Override
    public void emptyFridge() {
        for (int i = 0; i<20; i++){
            if (fridgeArray.get(i)!=null){
                fridgeArray.remove(fridgeArray.get(i));
            }
        }
    }

     @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem>itemsRemoved = new ArrayList<>();
        for(FridgeItem maybeExpired : fridgeArray) {
            if(maybeExpired.hasExpired()) {
                itemsRemoved.add(maybeExpired);
            }
        }
        for(FridgeItem removingItem:itemsRemoved) {
            fridgeArray.remove(removingItem);

        }
        return itemsRemoved;
    }
}

    
